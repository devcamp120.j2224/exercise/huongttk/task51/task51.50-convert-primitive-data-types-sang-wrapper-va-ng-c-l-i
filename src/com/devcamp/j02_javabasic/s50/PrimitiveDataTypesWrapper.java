package com.devcamp.j02_javabasic.s50;

public class PrimitiveDataTypesWrapper {
    public static void convertPrimitiveTypesToWrapperObjects() {
        // create primitive types
        byte myByte = 1;
        short myShortNumber = 2;
        int myIntNumber = 3;
        long myLongNumber = 4;
        float myFloatNumber = 5.0F;
        double myDoubleNumber = 6.2;
        boolean myBooleanNumber = false;
        char myCharNumber = 'B';
        //converts into wrapper objects
        Byte byeobj = myByte;
        Short shortobj = myShortNumber;
        Integer intobj = myIntNumber;
        Long longobj = myLongNumber;
        Float floatobj = myFloatNumber;
        Double doubleobj = myDoubleNumber;
        Boolean booleanobj = myBooleanNumber;
        Character charobj = myCharNumber;
        // in ra
        System.out.println("In ra gia tri cua object");
        System.out.println("Byte object: " + byeobj);
        System.out.println("Short object: " + shortobj);
        System.out.println("Integer object: " + intobj);
        System.out.println("Long object: " + longobj);
        System.out.println("Float object: " + floatobj);
        System.out.println("Double object: " + doubleobj);
        System.out.println("Boolean object: " + booleanobj);
        System.out.println("Character object: " + charobj);
    }
    public static void ConvertWrappeObjectsToPrimitiveTypes() {
         // creates objects of wrapper class
         byte myByte = 115;
         short myShortNumber = 216;
         int myIntNumber = 317;
         long myLongNumber = 418;
         float myFloatNumber = 59.0F;
         double myDoubleNumber = 60.2;
         boolean myBooleanNumber = true;
         char myCharNumber = 'D';
 
         Byte byeobj = myByte;
         Short shortobj = myShortNumber;
         Integer intobj = myIntNumber;
         Long longobj = myLongNumber;
         Float floatobj = myFloatNumber;
         Double doubleobj = myDoubleNumber;
         Boolean booleanobj = myBooleanNumber;
         Character charobj = myCharNumber;
        // converts into primitive types
        byte bytevalue = byeobj;
        short shortvalue = shortobj;
        int intvalue = intobj;
        long longvalue = longobj;
        float floatvalue = floatobj;
        double doublevalue = doubleobj;
        boolean booleanvalue = booleanobj;
        char charvalue = charobj;

        // print the primitive values
        System.out.println("In ra gia tri cua cac Primitive Data Type(kieu du lieu nguyen thuy)");
        System.out.println("Byte value: " + bytevalue);
        System.out.println("Short value: " + shortvalue);
        System.out.println("Integer value: " + intvalue);
        System.out.println("Long value: " + longvalue);
        System.out.println("Float value: " + floatvalue);
        System.out.println("Double value: " + doublevalue);
        System.out.println("Boolean value: " + booleanvalue);
        System.out.println("Character value: " + charvalue);
    }
    public static void main(String[] args) {
        PrimitiveDataTypesWrapper.convertPrimitiveTypesToWrapperObjects();
        PrimitiveDataTypesWrapper.ConvertWrappeObjectsToPrimitiveTypes();
    }
}
